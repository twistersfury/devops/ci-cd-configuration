# Twister's Fury Managed CI/CD Project
## Introduction

This project exists as a base template to be used on all projects. You may use it by adding the following to your `.gitlab-ci.yml` file:

```yaml
include:
    - project: 'twistersfury/devops/ci-cd-configuration'
      ref: 'v2.0.0'
      file:
          - '/docker.yml'
```

Replace `/docker.yml` with the appropriate Pre-Built Script.

## Pre-Built Scripts

### docker.yml

Used for Building a Single Docker Image

### helm-chart.yml

Used for Building a Helm Chart