## v3.0.1
- Fixing Variables...Forgot The Brackets
## v3.0.0
- Adding `DOCKER_MEMORY` and `DOCKER_SWAP` to define memory for buildx.
- Adjusting BuildX To Use AMD By Default (Instead of ARM/AMD)
- Removing 'Default' Build For ARM in Favor of Using `parallel`